import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.Timer;
import java.awt.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class AnimatorApp
  extends JFrame
{
  private static final long serialVersionUID = 1L;
  private JPanel contentPane;
  
  public static void main(String[] args)
  {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        try {
          AnimatorApp frame = new AnimatorApp();
          frame.setVisible(true);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }
  



  public AnimatorApp()
  {
    setDefaultCloseOperation(3);
    int ww = 450;int wh = 300;
    setBounds((ww) / 2, (wh) / 2, ww, wh);
    contentPane = new JPanel();
    setContentPane(contentPane);
    contentPane.setLayout(null);
  
    final AnimPanel kanwa = new AnimPanel();
    kanwa.setBounds(10, 11, 422, 219);
    contentPane.add(kanwa);
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
        kanwa.initialize();
      }
      
    });
    JButton btnAdd = new JButton("Add");
    btnAdd.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        kanwa.addFig();
      }
    });
    btnAdd.setBounds(10, 239, 80, 23);
    contentPane.add(btnAdd);
    
    JButton btnAnimate = new JButton("Animate");
    btnAnimate.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        kanwa.animate();
      }
    });
    btnAnimate.setBounds(100, 239, 80, 23);
    contentPane.add(btnAnimate);

  }
 }
